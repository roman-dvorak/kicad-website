+++
title = "SPICE Simulation"
tags = ["simulation", "spice"]
[menu.main]
    parent = "Discover"
    name   = "SPICE Simulation"
	weight = 5
+++

KiCad integrates the open source spice simulator http://ngspice.sourceforge.net/[ngspice] to provide simulation capability in graphical form through integration with the Schematic Editor.

<!--more-->

== Compatible with various spice models

The use of ngspice grants compatibility with existing models meant for:

- spice
- ltspice
- pspice
- hspice

== Schematic Integration

Design your simulation circuit in the schematic and launch the simulator directly from the editor

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="launch-sim.webm" type="video/webm">
        <source src="launch-sim.mp4" type="video/mp4">
    </video>
</div>
++++

== Configure simulations

Configure four standard simulation modes:

- AC Sweep
- DC Transfer
- Operating Point
- Transient

or define your own custom simulation

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="sim-parameters.webm" type="video/webm">
        <source src="sim-parameters.mp4" type="video/mp4">
    </video>
</div>
++++

== Run and probe simulations

Run simulations and use the probe tool to select nets to plot voltage and pins to plot current. 

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="run-probe.webm" type="video/webm">
        <source src="run-probe.mp4" type="video/mp4">
    </video>
</div>
++++

== Basic SPICE symbols

Both a standard SPICE library of symbols

image::lib-simspice.png[align=center, alt="Screenshot of simulation-spice library in symbol picker dialog"]

and PSPICE extended library symbols are included in KiCad's standard symbol library

image::lib-pspice.png[align=center, alt="Screenshot of pspice library in symbol picker dialog"]

NOTE: Due to licensing concerns, KiCad does not currently bundle any third party SPICE libraries. As a end user you are free to use any compatible SPICE libraries from manufacturers.
