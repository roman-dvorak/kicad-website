+++
title = "KiCad 7.0.2 Release"
date = "2023-04-16"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the second series 7 bug fix release.
The 7.0.2 stable version contains critical bug fixes and other minor improvements
since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.1 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/24[KiCad 7.0.2
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.2 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General

- Select correct face from a .ttc file. https://gitlab.com/kicad/code/kicad/-/issues/13314[#13314]
- Fix corrupt PDF plots on when fields contain parenthesis. https://gitlab.com/kicad/code/kicad/-/issues/14302[#14302]
- https://gitlab.com/kicad/code/kicad/-/commit/92fc4872e88336d3ba3d45b7981d552d67293b19[Fix settings import paths showing up twice on GTK builds].
- https://gitlab.com/kicad/code/kicad/-/commit/ba7598d90d17eedcbc93e0780ba3d78f30219506[Fix clashing error dialogs when can't load dynamic library].
- Ensure 3D shapes are always shown in footprint settings dialog. https://gitlab.com/kicad/code/kicad/-/issues/14371[#14371]
- https://gitlab.com/kicad/code/kicad/-/commit/3f90c895b91df05ce9e77aa91cac4f7c6c5d7c47[Prevent crash when changing color settings].
- Fix fix incorrect arc position when plotting GERBER files with offset. https://gitlab.com/kicad/code/kicad/-/issues/14402[#14402]
- Add file version backwards lockout for project file. https://gitlab.com/kicad/code/kicad/-/issues/14414[#14414]
- https://gitlab.com/kicad/code/kicad/-/commit/42b7aa64508e9db024422b26aea4c16f724158e6[Fix crash when reading or writing settings].
- Fix broken library nickname input when adding library table entry with add button. https://gitlab.com/kicad/code/kicad/-/issues/14291[#14291]
- https://gitlab.com/kicad/code/kicad/-/commit/e39550e77f94a9b2f6a71f4e7c85e90eaf3bb267[Update Clipper to version 1.2+].
- Fix image bounding box size. https://gitlab.com/kicad/code/kicad/-/issues/14432[#14432]
- Make library table edit dialog context menu activate and deactivate entries take effect immediately. https://gitlab.com/kicad/code/kicad/-/issues/14517[#14517]
- https://gitlab.com/kicad/code/kicad/-/commit/1f34883a277ac80c5ec20357ec7cbb96d50df547[Make symbol and footprint library trees sensitive to language change].
- https://gitlab.com/kicad/code/kicad/-/commit/35def3ab2e9fff09590aad7ebba27c74bab0b368[Fix possible event race condition on startup].
- Add hack to speed up font choice box widget creation. https://gitlab.com/kicad/code/kicad/-/issues/14277[#14277]
- https://gitlab.com/kicad/code/kicad/-/commit/f1f69c607efdc6d74ebda82b7523e05ee0df7e93[Avoid segfault when cancelling initial global library setup].

=== Schematic Editor

- Fix broken undo on alternate pin assignments. https://gitlab.com/kicad/code/kicad/-/issues/14100[#14100]
- Add missing sheet names to PDF plot. https://gitlab.com/kicad/code/kicad/-/issues/14233[#14233]
- Remove non-global labels from label properties dialog selection list. https://gitlab.com/kicad/code/kicad/-/issues/14319[#14319]
- Remove replace all accelerator key from find/replace dialog. https://gitlab.com/kicad/code/kicad/-/issues/14304[#14304]
- https://gitlab.com/kicad/code/kicad/-/commit/9d457dc0abf4f2848b250485614ab43dd2afc2f5[Create field name if user doesn't specify one].
- Fix sheet pin name case sensitivity issue. https://gitlab.com/kicad/code/kicad/-/issues/14415[#14415]
- Fix incorrect position of text for some rotations when plotting. https://gitlab.com/kicad/code/kicad/-/issues/14327[#14327]
- Ignore unknown attributes when importing CADSTAR schematic. https://gitlab.com/kicad/code/kicad/-/issues/13526[#13526]
- Do not duplicate signals when unfolding bus definitions. https://gitlab.com/kicad/code/kicad/-/issues/14269[#14269]
- Fix broken net-class label when using hierarchical sub-sheets. https://gitlab.com/kicad/code/kicad/-/issues/14494[#14494]
- Fix crash when changing net label to global label. https://gitlab.com/kicad/code/kicad/-/issues/14493[#14493]
- Ensure swapping global labels swaps inter-sheet references. https://gitlab.com/kicad/code/kicad/-/issues/14520[#14520]
- Allow bus elements to connect. https://gitlab.com/kicad/code/kicad/-/issues/14300[#14300]
- Fix broken symbol has changed in library ERC. https://gitlab.com/kicad/code/kicad/-/issues/14160[#14160]
- Search fields in labels. https://gitlab.com/kicad/code/kicad/-/issues/14075[#14075]

=== Spice Simulator

- Add missing .dc command to SPICE directives list. https://gitlab.com/kicad/code/kicad/-/issues/14215[#14215]
- Added PSPICE/LTSPICE JFET model parameters. https://gitlab.com/kicad/code/kicad/-/issues/12425[#12425]
- Handle underscore in parameter names. https://gitlab.com/kicad/code/kicad/-/issues/14308[#14308]
- Fix crash when transistor model is missing. https://gitlab.com/kicad/code/kicad/-/issues/14295[#14295]
- Ensure legend reflects gain/phase for AC small signal analyses. https://gitlab.com/kicad/code/kicad/-/issues/14301[#14301]
- https://gitlab.com/kicad/code/kicad/-/commit/1dc9583e065ae096718a11c07d7005369251ef8d[Don't attempt to load unknown plot types].
- Ignore extraneous LTSpice parameters for VDMOS models. https://gitlab.com/kicad/code/kicad/-/issues/14299[#14299]
- Separate legacy model name from SPICE parameters. https://gitlab.com/kicad/code/kicad/-/issues/13988[#13988]
- Do not include invalid simulation parameters. https://gitlab.com/kicad/code/kicad/-/issues/14369[#14369]
- Fix crash loading simulation with non-existent pin. https://gitlab.com/kicad/code/kicad/-/issues/14522[#14522]

=== Symbol Editor

- https://gitlab.com/kicad/code/kicad/-/commit/25f40566edd285c8ff6e578eb6def4b1d3d42467[Rix incorrect position of fields when loading a symbol from schematic].

=== Board Editor

- Fix crash for stale ratsnest connections. https://gitlab.com/kicad/code/kicad/-/issues/14254[#14254]
- Handle plotting of text on solder mask layer. https://gitlab.com/kicad/code/kicad/-/issues/14226[#14226]
- https://gitlab.com/kicad/code/kicad/-/commit/c0ddca12df792c9a15549cb127615b0aadf53406[Add Property Inspector support for dimension objects].
- Fix crash when manipulating zone corner. https://gitlab.com/kicad/code/kicad/-/issues/14265[#14265]
- Fix broken via placement on 45 degree tracks. https://gitlab.com/kicad/code/kicad/-/issues/14293[#14293]
- https://gitlab.com/kicad/code/kicad/-/commit/819a9da599da8bd9b997e399a53bf84ec62782cf[Fix crash when loading a library with a footprint containing a dimension].
- Fix contradicting number of warnings when updating board from schematic. https://gitlab.com/kicad/code/kicad/-/issues/14290[#14290].
- Do not include groups when not included in filter. https://gitlab.com/kicad/code/kicad/-/issues/14273[#14273]
- Fix crash when selecting grouped and ungrouped items. https://gitlab.com/kicad/code/kicad/-/issues/14347[#14347]
- Snap to grid on orthoganal dimension creation. https://gitlab.com/kicad/code/kicad/-/issues/13728[#13728]
- Fix "Locked Item Shadow" enable when changing presets. https://gitlab.com/kicad/code/kicad/-/issues/14381[#14381]
- Allow solder mask bridges between net-tie-group pads. https://gitlab.com/kicad/code/kicad/-/issues/14412[#14412]
- https://gitlab.com/kicad/code/kicad/-/commit/abb6404ab2d5ba3bb3984443999fd3ff1a6dfda4[Fix crash when importing Eagle board with layers that cannot be mapped].
- Fix broken silkscreen clipped by soldermask DRC. https://gitlab.com/kicad/code/kicad/-/issues/14417[#14417]
- Update allow-solder-mask-bridges when updating footprint from library. https://gitlab.com/kicad/code/kicad/-/issues/14422[#14422]
- https://gitlab.com/kicad/code/kicad/-/commit/f93fde15b7abc3350c65e72f6d0e833e4925cf9f[Use the correct locked shadow color].
- https://gitlab.com/kicad/code/kicad/-/commit/80b842a7040f590485f8524367d228c32748f430[Ensure the footprint is updated after new library selection in footprint viewer].
- Fix loading of rounded rectangle pads in CADSTAR importer. https://gitlab.com/kicad/code/kicad/-/issues/14445[#14445]
- Fix text position when moving text boxes. https://gitlab.com/kicad/code/kicad/-/issues/14452[#14452]
- Properly import layer stack up when importing CADSTAR board files. https://gitlab.com/kicad/code/kicad/-/issues/14443[#14443]
- https://gitlab.com/kicad/code/kicad/-/commit/85038c2b46545b83c3e5795915a8f3df29fab940[Prevent board setup dialog from being displayed multiple times].
- Add hole clearance to mounting hole keepouts for SPECCTRA export. https://gitlab.com/kicad/code/kicad/-/issues/14439[#14439]
- Save footprint position changed in properties panel. https://gitlab.com/kicad/code/kicad/-/issues/14348[#14348]
- Apply text knock out when exporting to VRML. https://gitlab.com/kicad/code/kicad/-/issues/14473[#14473]
- https://gitlab.com/kicad/code/kicad/-/commit/3de80d3b20d03a2c9f833eb31a6edc50e25522f1[Fix over zealous plot of negative silkscreen layer objects].
- Prevent copper zone fill connections from being too narrow. https://gitlab.com/kicad/code/kicad/-/issues/14130[#14130]
- Add DRC support for holes on inner corners of board outline. https://gitlab.com/kicad/code/kicad/-/issues/13437[#13437]
- Handle more pad shapes correctly when building differential pair gateways. https://gitlab.com/kicad/code/kicad/-/issues/1883[#1883]
- https://gitlab.com/kicad/code/kicad/-/commit/fb1cc720138f7bce43b18ba71188dddd8eb60122[Fix crash when importing empty PCAD polygons].

=== Footprint Editor

- Fix crash when inserting copied trace into footprint. https://gitlab.com/kicad/code/kicad/-/issues/14335[#14335]
- https://gitlab.com/kicad/code/kicad/-/commit/9466624f9da76e5bbe77d55945372679497a665a[Always export the currently loaded footprint].

=== Gerber Viewer

- https://gitlab.com/kicad/code/kicad/-/commit/3915e882a8da269cc9a7b1d7fc41a8f13efc3c5e[Fix handling of variable redefinitions].

=== 3D Viewer

- Avoid drawing invalid arcs in 3D viewer. https://gitlab.com/kicad/code/kicad/-/issues/14271[#14271]

=== Python scripting

- Load the global fp-lib-table before running the DRC. https://gitlab.com/kicad/code/kicad/-/issues/13815[#13815]
- Remove board_item duplicate method. https://gitlab.com/kicad/code/kicad/-/issues/14460[#14460]

=== Command Line Interface

- Fix incorrect units in user layer when exporting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/14170[#14170]
- https://gitlab.com/kicad/code/kicad/-/commit/434161687ed0d426cb0649c7a4cee04cb5744548[Fix crash due to dialogs buried in the pcb parser].
- Do not ignore the "--separate_files" option when generating drill files. https://gitlab.com/kicad/code/kicad/-/issues/14454[#14454]
- Allow using UI and board file layer names. https://gitlab.com/kicad/code/kicad/-/issues/14455[#14455]

=== Windows

- https://gitlab.com/kicad/code/kicad/-/commit/4e775a4090d03d123a0b0927a4f3b44d635169a3[Update vcpkg curl and Python versions].
- https://gitlab.com/kicad/code/kicad/-/commit/bc4878d71e9e7042de372104e2e3042913c568f1[Bump ngspice build version to 40].

=== macOS

- https://gitlab.com/kicad/code/kicad/-/commit/89e78c0276e209f8a0da46a00c6a718fac09dd7f[Fix crash on quit when timed infobar is still present].

=== Linux
- https://gitlab.com/kicad/code/kicad/-/commit/d3f449492545da66598f78684504a139fd8e521e[Make version info work without lsb-release installed].
