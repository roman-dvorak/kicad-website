+++
title = "QUBIK"
projectdeveloper = "Libre Space Foundation"
projecturl = "https://libre.space/projects/qubik/"
"made-with-kicad/categories" = ["Space"]
+++

QUBIK-1 and QUBIK-2 are 2 PocketQube satellites designed, developed and integrated by Libre Space Foundation. They were expected to be short-lived and were tasked to perform Launch and Early Operations Phase (LEOP) Satellite Identification and Tracking experiments. 
